Source: liblayout-manager-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Angel Abad <angel@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13), libmodule-install-perl
Build-Depends-Indep: libgeometry-primitive-perl,
                     libgraphics-primitive-perl,
                     libmoose-perl,
                     perl
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/liblayout-manager-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/liblayout-manager-perl.git
Homepage: https://metacpan.org/release/Layout-Manager

Package: liblayout-manager-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libgeometry-primitive-perl,
         libgraphics-primitive-perl,
         libmoose-perl
Description: module for managing layout of graphical components
 Layout::Manager is a Perl module that provides a framework for working with
 layout managers, which are classes that determine the size and position of
 arbitrary components within a container. A few managers are provided for
 reference purposes, but this module is intended to serve as a basis for other
 implementations.
 .
 This module uses Graphics::Primitive::Container as a source of its components
 (see libgraphics-primitive-perl for details).
